<?php
namespace Sportily\Laravel\Facades;

class Contacts extends \Illuminate\Support\Facades\Facade {

    protected static function getFacadeAccessor() {
        return 'sportily.endpoints.Contacts';
    }

}
