<?php
namespace Sportily\Laravel\Facades;

class Clubs extends \Illuminate\Support\Facades\Facade {

    protected static function getFacadeAccessor() {
        return 'sportily.endpoints.Clubs';
    }

}
